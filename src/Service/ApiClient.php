<?php


namespace Apcenuu\ExampleClient\Service;


use Apcenuu\ExampleClient\Object\Comment;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use JMS\Serializer\SerializerInterface;

class ApiClient
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var string
     */
    public $baseUrl = 'http://example.com/';
    /**
     * @var SerializerInterface
     */
    private $serializer;

    public function __construct(Client $client, SerializerInterface $serializer)
    {
        $this->client = $client;
        $this->serializer = $serializer;
    }

    /**
     * @return Comment[]
     * @throws GuzzleException
     */
    public function getComments()
    {
        $response = $this->client->get($this->baseUrl . 'comments');
        $response = $response->getBody()->getContents();

        return $this->serializer->deserialize($response, 'array<' . Comment::class . '>', 'json');

    }

    /**
     * @param $comment
     * @return Comment
     * @throws GuzzleException
     */
    public function addComment($comment)
    {
        $response = $this->client->post($this->baseUrl . 'comment', [
            'json' => $comment,
        ]);

        return $this->serializer->deserialize($response->getBody()->getContents(), Comment::class, 'json');
    }

    public function updateComment($comment, $id)
    {
        $response = $this->client->put($this->baseUrl . 'comment/' . $id, [
            'json' => $comment,
        ]);
        return $this->serializer->deserialize($response->getBody()->getContents(), Comment::class, 'json');
    }
}