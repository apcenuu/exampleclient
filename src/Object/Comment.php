<?php


namespace Apcenuu\ExampleClient\Object;

use JMS\Serializer\Annotation as JMS;


class Comment
{
    /**
     * @var int $id
     *
     * @JMS\Type("integer")
     */
    private $id;

    /**
     * @var string $name
     *
     * @JMS\Type("string")
     */
    private $name;

    /**
     * @var string $text
     *
     * @JMS\Type("string")
     */
    private $text;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text): void
    {
        $this->text = $text;
    }


}