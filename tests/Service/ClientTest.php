<?php


namespace Apcenuu\ExampleClient\Tests\Service;

use Apcenuu\ExampleClient\Object\Comment;
use Apcenuu\ExampleClient\Service\ApiClient;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use JMS\Serializer\SerializerBuilder;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;

class ClientTest extends TestCase
{
    /**
     *
     * @dataProvider commentListProvider
     * @throws GuzzleException
     */
    public function testGet($object)
    {
        $apiClient = $this->initClient($object);
        $response = $apiClient->getComments();

        Assert::assertTrue(count($response) > 0);

        /** @var Comment $comment */
        foreach ($response as $comment) {
            Assert::assertIsString($comment->getName());
            Assert::assertIsString($comment->getText());

        }
    }

    /**
     * @dataProvider addCommentProvider
     * @throws GuzzleException
     */
    public function testPost($object)
    {
        $apiClient = $this->initClient($object);

        $comment = [
            'name' => 'Arseniy',
            'text' => 'My comment',
        ];

        $responseComment = $apiClient->addComment($comment);

        Assert::assertIsString($responseComment->getName());
        Assert::assertIsString($responseComment->getText());
        Assert::assertIsInt($responseComment->getId());
    }

    /**
     * @dataProvider updateCommentProvider
     */
    public function testPut($object)
    {
        $apiClient = $this->initClient($object);

        $comment = [
            'name' => 'Arseniy',
            'text' => 'My comment',
        ];

        $responseComment = $apiClient->updateComment($comment, 1);

        Assert::assertIsString($responseComment->getName());
        Assert::assertIsString($responseComment->getText());
        Assert::assertIsInt($responseComment->getId());
    }

    /**
     * @return array
     */
    public function addCommentProvider()
    {
        return [
            [
                [
                    'id' => 1,
                    'name' => 'Arseniy',
                    'text' => 'sample text'
                ]
            ]
        ];
    }

    /**
     * @return array
     */
    public function updateCommentProvider()
    {
        return [
            [
                [
                    'id' => 1,
                    'name' => 'Arseniy',
                    'text' => 'sample text'
                ]
            ]
        ];
    }
    
    /**
     * @return array
     */
    public function commentListProvider()
    {
        return [
            [
                [
                    [
                        'id' => 1,
                        'name' => 'Arseniy',
                        'text' => 'sample text'
                    ]
                ]
            ],
            [
                [
                    [
                        'id' => 1,
                        'name' => 'Arseniy',
                        'text' => 'test'
                    ],
                    [
                        'id' => 2,
                        'name' => 'Ivan',
                        'text' => 'Mda'
                    ],
                    [
                        'id' => 3,
                        'name' => 'Timofey',
                        'text' => 'Okay'
                    ]
                ]
            ],
        ];
    }
    
    private function initClient($object)
    {
        $mock = new MockHandler([
            new Response(200, ['X-Foo' => 'Bar'], json_encode($object)),
            new RequestException('Error Communicating with Server', new Request('GET', 'test'))
        ]);

        $handlerStack = HandlerStack::create($mock);
        $client = new Client(['handler' => $handlerStack]);

        $serializer = SerializerBuilder::create()->addMetadataDir('src/Resources/config/serializer')->build();
        return new ApiClient($client, $serializer);
    }
}